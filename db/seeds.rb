# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


require 'csv'

Diagnostic.delete_all
open("/vagrant/projects/ProjectCIS4339/csv/dermacodes.csv") do |code|
  code.read.each_line do |code|
    code, fee = code.chomp.split(",")
    Diagnostic.create!( code: code, fee: fee)
  end
end

State.delete_all
open("/vagrant/projects/ProjectCIS4339/csv/states.csv") do |s|
  s.read.each_line do |s|
    state = s
    State.create!(state: state)
  end
end

Insurance.delete_all
open("/vagrant/projects/ProjectCIS4339/csv/insurances.csv") do |i|
  i.read.each_line do |i|
    name = i
    Insurance.create!(name: name)
  end
end
