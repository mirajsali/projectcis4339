class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :address
      t.string :city
      t.integer :state_id
      t.string :zipcode
      t.string :phone
      t.integer :insurance_id
      t.integer :user_id

      t.timestamps
    end
  end
end
