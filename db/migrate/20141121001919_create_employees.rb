class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :address
      t.string :city
      t.integer :state_id
      t.string :zipcode
      t.string :phone
      t.string :socialsecurity
      t.integer :user_id

      t.timestamps
    end
  end
end
