class CreateDiagnostics < ActiveRecord::Migration
  def change
    create_table :diagnostics do |t|
      t.string :code
      t.decimal :fee

      t.timestamps
    end
  end
end
