require 'csv'

namespace :csv do

  desc "Import CSV Files"
  task :import => :environment do
    csv_file_path = 'db/states.csv'

    CSV.foreach(csv_file_path) do |s|
      State.create({:state => s})
    end
  end
end