json.array!(@employees) do |employee|
  json.extract! employee, :id, :address, :city, :state_id, :zipcode, :phone, :socialsecurity, :user_id
  json.url employee_url(employee, format: :json)
end
