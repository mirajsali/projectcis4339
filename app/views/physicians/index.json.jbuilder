json.array!(@physicians) do |physician|
  json.extract! physician, :id, :address, :city, :state_id, :zipcode, :phone, :socialsecurity, :user_id
  json.url physician_url(physician, format: :json)
end
