class AppointmentssDatatable
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: Appointment.count,
        iTotalDisplayRecords: appointments.total_entries,
        aaData: data
    }
  end

  private

  def data
    appointments.map do |appointment|
      [
          link_to(appointment.name, appointment),
          h(appointment.category),
          h(appointment.released_on.strftime("%B %e, %Y")),
          number_to_currency(appointment.price)
      ]
    end
  end

  def products
    @appointments ||= fetch_appointments
  end

  def fetch_products
    appointments = Appointment.order("#{sort_column} #{sort_direction}")
    appointments = appointments.page(page).per_page(per_page)
    if params[:sSearch].present?
      appointments = appointments.where("name like :search or category like :search", search: "%#{params[:sSearch]}%")
    end
    appointments
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[name category released_on price]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end