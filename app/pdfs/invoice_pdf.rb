require 'prawn'

class InvoicePdf < Prawn::Document
  def initialize(appointment, view)
    super(top_margin: 65)
    @appointment = appointment
    @view = view
    company
    invoice_number
    appointment_items
    message
  end

  def invoice_number
    move_up 25
    text "Invoice: \##{@appointment.id}",
         size:14, style: :bold
    text "Date: #{(Date.current).in_time_zone("Central Time (US & Canada)").strftime("%x")}",
         size:14, style: :bold
    text "Generated: #{Time.now.in_time_zone("Central Time (US & Canada)").strftime("%I:%M %P")}",
         size:14, style: :bold
    move_down 20
    patient_info
    appointment_summary
  end

  def appointment_summary
    move_up 120
    summary = [["Appointment Summary"],
               ["Physician: #{@appointment.physician.name}"],
               ["Phone No.: #{phone(@appointment.physician.phone)}"],
               ["#{@appointment.date.strftime("%A, %B %d %I:%M%P %G")}"],
               ["Total Due: $#{@appointment.diagnostic.fee}0"]
    ]
    table summary, position: :right do
      rows(0).font_style = :bold
      rows(4).font_style = :bold
    end
  end

  def patient_info
    move_down 10
    info = [["Bill To:"],
            ["#{@appointment.patient.name}"],
            ["#{@appointment.patient.address}"],
            ["#{@appointment.patient.city}, #{@appointment.patient.state.state} #{@appointment.patient.zipcode}"],
            ["#{phone(@appointment.patient.phone)}"]
    ]
    table info do
      row(0).font_style = :bold
      row(1).border_lines = [:solid]
    end
  end

  def appointment_items
    move_down 30
    table appointment_item_rows do
      rows(0).font_style = :bold
      rows(2).font_style = :bold
      column(1..2).width = 180
      column(3).width = 70
      self.row_colors = ["8bb7ed","FFFFFF"]
    end
  end

  def appointment_item_rows
    [["Date","Appointment Reason","Diagnosis","Fee"],
     ["#{@appointment.date.strftime("%A, %B %d %Y")}","#{@appointment.reason}","#{@appointment.diagnostic.code}","#{@appointment.diagnostic.fee}0"],
     ["","","Total Amount Due:","$#{@appointment.diagnostic.fee}0"]
    ]
  end

  def phone(num)
    @view.number_to_phone(num)
  end

  def message
    move_down 300
    text "Thanks for choosing DermaCare One, the #1 Dermatology Clinic in Houston.",
         align: :center
  end

  def company
    text "DERMACARE ONE",
      style: :bold,
      size: 20,
      align: :right,
      text_color: ["8bb7ed"]
    text "#1 in Houston!",
      size: 12,
      style: :italic,
      align: :right
  end

end
