class Appointment < ActiveRecord::Base

  belongs_to :patient
  belongs_to :physician
  belongs_to :diagnostic
  belongs_to :status

  validates_uniqueness_of :date, scope: [:physician_id], :message=>"and time is already taken with this physician. Please select another time or physician."

  DateTime.now.new_offset(0)

  after_initialize :defaults

  def defaults
    self.status_id ||= 1 if self.status_id.nil?
    self.diagnostic_id ||= 2029 if self.diagnostic_id.nil?
  end

  def self.totalappointment
    if (:status_id == 4)
    @totalapps = Appointment.count
    end
  end

end
