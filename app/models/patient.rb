class Patient < ActiveRecord::Base

  belongs_to :insurance
  has_many :physicians , :through => :appointments
  has_many :appointments
  has_one :user
  belongs_to :state

  def self.emailcheck
    if Patient.where(userid: self.userid).exists? then
      errors.add(:userid,'This email is already in use. Please use another.')
    end
  end

  def self.emailval
    validates_uniqueness_of :email
  end

  # Donor.joins(:donations).where(:id => params[:id]).sum('value'))


end