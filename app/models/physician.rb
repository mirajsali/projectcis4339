class Physician < ActiveRecord::Base

  has_one :user
  has_many :patients, :through => :appointments
  has_many :appointments
  belongs_to :state

  def self.emailcheck
    if Patient.where(userid: self.userid).exists? then
      errors.add(:userid,'This email is already in use. Please use another.')
    end
  end
end
