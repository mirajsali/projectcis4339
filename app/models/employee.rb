class Employee < ActiveRecord::Base

  has_one :user
  belongs_to :state

  def self.emailcheck
    if Patient.where(userid: self.userid).exists? then
      errors.add(:userid,'This email is already in use. Please use another.')
    end
  end
end