class User < ActiveRecord::Base

  belongs_to :patient
  belongs_to :physician
  belongs_to :employee

  enum role: [:admin, :physician, :patient, :employee]
  after_initialize :set_default_role, :if => :new_record?

  def set_default_role
    self.role ||= :patient
  end

  def self.emailval
    validates_uniqueness_of :email
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
