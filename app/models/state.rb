class State < ActiveRecord::Base

  has_many :patients
  has_many :physicians
  has_many :employees

end
