class AppointmentsController < ApplicationController
  before_action :set_appointment, only: [:show, :edit, :update, :destroy]


  def index
    @appointments = Appointment.all
    @appointments = Appointment.includes(:status).order(:status_id, :date).paginate(:page => params[:page], :per_page => 5)
    respond_to do |format|
      format.html
      format.json { render json: AppointmentsDatatable.new(view_context) }
    end
  end

  def show
    respond_to do |format|
      format.html
      format.pdf do
        pdf = InvoicePdf.new(@appointment, view_context)
        send_data pdf.render, filename: "Invoice ##{@appointment.id}.pdf",
                  type: "application/pdf",
                  disposition: "inline"
      end
    end
  end

  def new
    @appointment = Appointment.new

  end

  def edit
  end

  def create
    @appointment = Appointment.new(appointment_params)
    @appointment.save
    respond_to do |format|
      if @appointment.save
        format.html { redirect_to @appointment, notice: 'Appointment was successfully created.' }
        format.json { render :show, status: :created, location: @appointment }
      else
        format.html { render :new }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end

  end

  def update
    @appointment.update(appointment_params)
    respond_to do |format|
      if @appointment.update(appointment_params)
        format.html { redirect_to @appointment, notice: 'Appointment was successfully updated.' }
        format.json { render :show, status: :ok, location: @appointment }
      else
        format.html { render :edit }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @appointment.destroy
    respond_to do |format|
      format.html { redirect_to appointments_url, notice: 'Appointment was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def completed
    @appointments = Appointment.all
    respond_to do |format|
      format.html
      format.json { render json: AppointmentsDatatable.new(view_context) }
    end
  end


  private
    def set_appointment
      @appointment = Appointment.find(params[:id])
    end

    def appointment_params
      params.require(:appointment).permit(:patient_id, :physician_id, :date, :reason, :notes, :diagnostic_id, :status_id)
    end

end

