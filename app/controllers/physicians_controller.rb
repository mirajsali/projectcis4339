class PhysiciansController < ApplicationController
  before_action :set_physician, only: [:show, :edit, :update, :destroy]



  def index
    @physicians = Physician.all
    @physicians = Physician.order(:name).paginate(:page => params[:page], :per_page => 15)
  end

  def show
    @physician = Physician.find(params[:id])
  end

  def new
    @physician = Physician.new
  end

  def edit
  end

  def create
    @physician = Physician.new(physician_params)
    @physician.save
    respond_to do |format|
      if @physician.save
        format.html { redirect_to @physician, notice: 'Physician was successfully created.' }
        format.json { render :show, status: :created, location: @physician }
      else
        format.html { render :new }
        format.json { render json: @physician.errors, status: :unprocessable_entity }
      end
    end

  end

  def update
    @physician.update(physician_params)
    respond_to do |format|
      if @physician.update(physician_params)
        format.html { redirect_to @physician, notice: 'Physician was successfully updated.' }
        format.json { render :show, status: :ok, location: @physician }
      else
        format.html { render :edit }
        format.json { render json: @physician.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @physician.destroy
    respond_to do |format|
      format.html { redirect_to physicians_url, notice: 'Physician was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    def set_physician
      @physician = Physician.find(params[:id])
    end

    def physician_params
      params.require(:physician).permit(:name, :address, :city, :state_id, :zipcode, :phone, :user_id)
    end
end
