class DiagnosticsController < ApplicationController
  before_action :set_diagnostic, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @diagnostics = Diagnostic.all
    @diagnostics = Diagnostic.paginate(:page => params[:page], :per_page => 20)

    respond_with(@diagnostics)
  end

  def show
    respond_with(@diagnostic)
  end

  def new
    @diagnostic = Diagnostic.new
    respond_with(@diagnostic)
  end

  def edit
  end

  def create
    @diagnostic = Diagnostic.new(diagnostic_params)
    @diagnostic.save
    respond_with(@diagnostic)
  end

  def update
    @diagnostic.update(diagnostic_params)
    respond_with(@diagnostic)
  end

  def destroy
    @diagnostic.destroy
    respond_with(@diagnostic)
  end

  private
    def set_diagnostic
      @diagnostic = Diagnostic.find(params[:id])
    end

    def diagnostic_params
      params.require(:diagnostic).permit(:code, :fee)
    end
end
